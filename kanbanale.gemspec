# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

Gem::Specification.new do |s|
  s.name          = 'kanbanale'
  s.version       = '0.1.1'
  s.summary       = 'Simple static HTML kanban board generator'
  s.description   = <<~DESC
    kanbanale is a static site generator for simple kanban boards.
    It outputs a static HTML board (no JS) from a text-based YAML definition.
  DESC
  s.authors       = ['Fabio Pesari']
  s.homepage      = 'https://gitlab.com/fpesari/kanbanale'
  s.files         = %w[.rubocop.yml kanbanale.gemspec LICENSE bin/kanbanale
                       README.md static/css/kanbanale.css static/css/toast.css
                       templates/index.erb templates/kanbanale.yaml]
  s.executables   = %w[kanbanale]
  s.license       = 'AGPL-3.0+'
end
