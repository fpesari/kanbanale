# kanbanale - Simple static HTML kanban board generator

kanbanale is a static site generator for simple kanban boards.
It outputs a static HTML board, no JS, from a text-based YAML definition.

I wrote it because I was looking for an easy way to visualize
task priorities, without running server-side or JavaScript code.

## Installation

You can install kanbanale like any Rubygem:

```
gem build kanbanale.gemspec
gem install kanbanale*.gemspec --user
```

## Usage

In order to use kanbanale, you need to know YAML.

First, generate a sample YAML file by running
`kanbanale init kanbanale.yaml`.

Fit the schema of the generated YAML file to your needs, then run
`kanbanale create kanbanale.yaml website/` to generate a directory called
`website` (or whatever you call it) which contains the whole static website.

Load it to a web server, instant cheapo kanban board.

To update it (so that your modifications to the CSS file are not affected),
run `kanbanale update kanbanale.yaml website/`

### Customization

This program doesn't try to do any fancy declarative customization because
it's the same thing as writing CSS, so in order to customize looks,
you have to modify `static/css/kanbanale.css`.

kanbanale takes the names for CSS div ids from their names in the
YAML file, so that `My ideas` becomes `#my-ideas`.

By default, it already provides a style for the most common type of
kanban board, so most people who use it won't have to touch CSS at all.

Thanks to Daniel Eden for Toast, by the way, as it provides the lightweight
grid on which kanbanale is based.

## License

Everything in this repository is released under the
GNU Affero General Public License, version 3 or any later version, unless
otherwise specified (and even in that case, it's all free software).
